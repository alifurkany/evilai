const { isMainThread, parentPort, workerData } = require("worker_threads");

if (isMainThread) {
	console.log("This file is not supposed to be ran directly");
	process.exit(0);
}

// Bot thread
const Discord = require("discord.js");
const Voice = require("@discordjs/voice");
const client = new Discord.Client({
	intents: ["GUILDS", "GUILD_VOICE_STATES"],
});
const fs = require("fs");

/** @type {Voice.VoiceConnection} */
let conn = null;
const player = Voice.createAudioPlayer();

parentPort.on("message", async (message) => {
	switch (message.type) {
		case "join":
			let channel = await client.channels.fetch(message.channel);
			if (channel && channel.type === "GUILD_VOICE") {
				if (conn) conn.disconnect();
				conn = Voice.joinVoiceChannel({
					channelId: channel.id,
					guildId: channel.guild.id,
					adapterCreator: channel.guild.voiceAdapterCreator,
				});
				conn.subscribe(player);
				parentPort.postMessage({
					type: "joined",
					channel: channel.name,
				});
			} else {
				parentPort.postMessage({
					type: "error",
					message: "Invalid channel",
				});
			}
			break;
		case "play":
			if (conn && conn.state.status === "ready") {
				let resource = Voice.createAudioResource(message.file);
				player.play(resource);
				parentPort.postMessage({
					type: "playing",
					file: message.file,
				});
			} else {
				parentPort.postMessage({
					type: "error",
					message: "Not connected to a voice channel",
				});
			}
			break;
		case "stop":
			if (player.state.status === "playing") {
				player.stop();
				parentPort.postMessage({
					type: "stopped",
				});
			} else {
				parentPort.postMessage({
					type: "error",
					message: "Not playing anything",
				});
			}
			break;
		case "pause":
			if (player.state.status === "playing") {
				player.pause();
				parentPort.postMessage({
					type: "paused",
				});
			} else if (player.state.status === "paused") {
				parentPort.postMessage({
					type: "error",
					message: "Already paused",
				});
			}
			break;
		case "resume":
			if (player.state.status === "paused") {
				player.unpause();
				parentPort.postMessage({
					type: "resumed",
				});
			} else if (player.state.status === "playing") {
				parentPort.postMessage({
					type: "error",
					message: "Already playing",
				});
			}
			break;
		case "disconnect":
			if (conn) {
				conn.disconnect();
				conn = null;
				parentPort.postMessage({
					type: "disconnected",
				});
			} else {
				parentPort.postMessage({
					type: "error",
					message: "Not connected to a voice channel",
				});
			}
			break;
		default:
			break;
	}
});

client.login(workerData.token);
