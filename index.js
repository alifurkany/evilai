// A discord bot that can play files from the host machine.
// Main thread
// Bot is not in the main thread

require("dotenv").config();
const fs = require("fs");
const { Worker } = require("worker_threads");
const inquirer = require("inquirer");

(async () => {
	const ui = new inquirer.ui.BottomBar();
	// Start the bot worker
	const bot = new Worker("./bot.js", { workerData: { token: process.env.BOT_TOKEN } });

	// Listen for inquiries
	// Types of inquiries:
	// - `join`: Join a voice channel
	// - `play`: Play a file
	// - `stop`: Stop playing
	// - `pause`: Pause playing
	// - `resume`: Resume playing
	// - `volume`: Change the volume
	// - `disconnect`: Disconnect from the voice channel
	// - `exit`: Exit the program

	bot.on("message", async (message) => {
		if (message.type === "joined") {
			ui.updateBottomBar(`Joined ${message.channel}`);
		} else if (message.type === "disconnected") {
			ui.updateBottomBar(`Disconnected`);
		} else if (message.type === "playing") {
			ui.updateBottomBar(`Playing ${message.file}`);
		} else if (message.type === "stopped") {
			ui.updateBottomBar(`Stopped`);
		} else if (message.type === "error") {
			ui.updateBottomBar(`${message.message}\n`);
		}
	});

	function inquire() {
		inquirer
			.prompt([
				{
					type: "list",
					name: "inquiry",
					message: "What would you like to do?",
					choices: [
						"Join a voice channel",
						"Play a file",
						"Stop playing",
						"Pause playing",
						"Resume playing",
						"Change the volume",
						"Disconnect from the voice channel",
						"Exit the program",
					],
				},
			])
			.then(async (answers) => {
				switch (answers.inquiry) {
					case "Join a voice channel":
						return inquirer
							.prompt([
								{
									type: "input",
									name: "channel",
									message: "What channel would you like to join?",
								},
							])
							.then(async (answers) => {
								bot.postMessage({
									type: "join",
									channel: answers.channel,
								});
							});
						break;
					case "Play a file":
						return inquirer
							.prompt([
								{
									type: "input",
									name: "file",
									message: "What file would you like to play?",
									validate: (value) => {
										if (!fs.existsSync(value)) {
											return "File does not exist";
										}
										return true;
									},
								},
							])
							.then(async (answers) => {
								bot.postMessage({
									type: "play",
									file: answers.file,
								});
							});
						break;
					case "Stop playing":
						bot.postMessage({
							type: "stop",
						});
						break;
					case "Pause playing":
						bot.postMessage({
							type: "pause",
						});
						break;
					case "Resume playing":
						bot.postMessage({
							type: "resume",
						});
						break;
					case "Change the volume":
						return inquirer
							.prompt([
								{
									type: "number",
									name: "volume",
									message: "What volume would you like to set?",
									validate: (value) => {
										if (value < 0 || value > 100) {
											return "Volume must be between 0 and 100";
										}
										return true;
									},
								},
							])
							.then(async (answers) => {
								bot.postMessage({
									type: "volume",
									volume: answers.volume,
								});
							});
						break;
					case "Disconnect from the voice channel":
						bot.postMessage({
							type: "disconnect",
						});
						break;
					case "Exit the program":
						process.exit(0);
						break;
				}
			})
			.then(() => {
				console.clear();
				inquire();
			});
	}
	inquire();
})();
